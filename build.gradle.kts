group = "org.example"
version = "0.1-SNAPSHOT"

plugins {
    kotlin("multiplatform") version "1.4.10"
}

kotlin {
    linuxX64 {
        compilations.getByName("main") {
            cinterops.create("libonion") {
                val homeDir = System.getProperty("user.home")
                includeDirs("$homeDir/libonion-0.8/include/onion")
                extraOpts("-libraryPath", "$homeDir/libonion-0.8/lib")
            }
        }
        binaries {
            executable("serverless_platform") {
                entryPoint = "org.example.serverlessPlatform.main"
            }
        }
    }
}
