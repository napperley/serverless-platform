@file:Suppress("EXPERIMENTAL_API_USAGE", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.example.serverlessPlatform

import platform.posix.SIGINT
import platform.posix.SIGTERM
import platform.posix.signal
import kotlinx.cinterop.staticCFunction

fun main() {
    signal(SIGINT, staticCFunction(::shutdownServer))
    signal(SIGTERM, staticCFunction(::shutdownServer))
    startServer(port = 5000)
}
