@file:Suppress("EXPERIMENTAL_API_USAGE")

package org.example.serverlessPlatform

import kotlinx.cinterop.CPointer
import kotlinx.cinterop.COpaquePointer
import kotlinx.cinterop.toKString
import libonion.*
import platform.posix.system

internal fun homeRoute(
    @Suppress("UNUSED_PARAMETER") userData: COpaquePointer,
    @Suppress("UNUSED_PARAMETER") req: CPointer<onion_request>,
    resp: CPointer<onion_response>
): UInt {
    initRuntimeIfNeeded()
    changeContentType(resp, "text/html")
    onion_response_write0(resp, "<h1>Serverless Platform</h1>")
    return HTTP_OK
}

internal fun callFuncRoute(
    @Suppress("UNUSED_PARAMETER") userData: COpaquePointer,
    @Suppress("UNUSED_PARAMETER") req: CPointer<onion_request>,
    resp: CPointer<onion_response>
): UInt {
    initRuntimeIfNeeded()
    val funcName = onion_request_get_query(req, "funcName")?.toKString() ?: ""
    val getMethod = (onion_request_get_flags(req) and OR_METHODS) == OR_GET
    val result = when {
        !getMethod -> HTTP_METHOD_NOT_ALLOWED
        funcName.isEmpty() -> HTTP_BAD_REQUEST
        callFunction(funcName) != 0 -> HTTP_INTERNAL_ERROR
        else -> HTTP_OK
    }
    val body = when {
        result == HTTP_METHOD_NOT_ALLOWED -> ""
        funcName.isEmpty() -> "Function name cannot be empty!"
        result == HTTP_INTERNAL_ERROR -> "An error was encountered when calling the function."
        else -> "Calling $funcName (from $functionsDir)..."
    }
    onion_response_write0(resp, body)
    println("HTTP Response Code: $result")
    changeContentType(resp, "text/plain")
    onion_response_set_code(resp, result.toInt())
    return result
}

private fun callFunction(funcName: String) = system("$functionsDir/$funcName")
