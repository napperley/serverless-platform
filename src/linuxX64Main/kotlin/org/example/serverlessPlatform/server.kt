@file:Suppress("EXPERIMENTAL_API_USAGE")

package org.example.serverlessPlatform

import libonion.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.staticCFunction
import kotlinx.cinterop.toKString
import kotlin.system.exitProcess
import platform.posix.getenv

private var server: CPointer<onion>? = null
internal val functionsDir = "${getenv("HOME")?.toKString()}/.serverless_platform/functions"

internal fun startServer(host: String = "localhost", port: Int) {
    println("Starting Serverless Platform...")
    server = onion_new(O_POOL.toInt())
    setupServer(host, port)
    onion_listen(server)
}

private fun setupServer(host: String, port: Int) {
    onion_set_hostname(server, host)
    onion_set_port(server, "$port")
    val urls = onion_root_url(server)
    onion_url_add(url = urls, regexp = "", handler_f = staticCFunction(::homeRoute))
    onion_url_add(url = urls, regexp = "callFunc", handler_f = staticCFunction(::callFuncRoute))
}

internal fun shutdownServer(@Suppress("UNUSED_PARAMETER") signal: Int) {
    println("Exiting...")
    if (server != null) {
        onion_listen_stop(server)
        onion_free(server)
    }
    exitProcess(0)
}

internal fun changeContentType(resp: CPointer<onion_response>, contentType: String) {
    onion_response_set_header(res = resp, key = "Content-Type", value = contentType)
}
